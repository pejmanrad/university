﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace University.Classes
{
    public static class checkPDF
    {
        public static bool isPdf(this HttpPostedFileBase postedFile) { 
            if (postedFile.ContentType.ToLower() != "application/pdf")
            {
                return false;
            }

            //-------------------------------------------
            //  Check the image extension
            //-------------------------------------------
            if(Path.GetExtension(postedFile.FileName).ToLower() != ".pdf")
            {
                return false;
            }
            return true;
        }
    }
}