﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace University.Classes
{
    public class ChangeText
    {
        public static string changeNumber(string txt)
        {
            string result="";
            foreach (var item in txt)
            {
                switch (item)
                {
                    case '۱':
                        result += '1';
                        break;

                    case '۲':
                        result += '2';
                        break;
                    case '۳':
                        result += '3';
                        break;
                    case '۴':
                        result += '4';
                        break;
                    case '۵':
                        result += '5';
                        break;
                    case '۶':
                        result += '6';
                        break;
                    case '۷':
                        result += '7';
                        break;
                    case '۸':
                        result += '8';
                        break;
                    case '۹':
                        result += '9';
                        break;
                    case '۰':
                        result += '0';
                        break;
                    default:
                        result += item;
                        break;
                }

            }
            return result;
        }

        public static string changeText(string txt)
        {
            string res = txt;
          var Text=  txt.Split(' ', '-', '/', '.', '،');
            foreach (var item in Text)
            {
                if (item[0]=='آ')
                {
                    string word="ا";
                    for (int i = 1; i < item.Length; i++)
                    {
                        word += item[i];
                    }
                    res += "," + word;
                }
                if (item[0] == 'ا')
                {
                    string word = "آ";
                    for (int i = 1; i < item.Length; i++)
                    {
                        word += item[i];
                    }
                    res += "," + word;
                }
            }
            return res;
        }
    }
}