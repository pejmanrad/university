﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using University.Models;

namespace University.Classes
{


    public class Search
    {

        public static List<Pamphlet> advSearch(string Text)
        {
            DBEntities db = new DBEntities();
            Text = ChangeText.changeNumber(Text);

            var txt = Text.ToLower().Split(' ', '-', '/', '.', ',', '،');

            var list1 = db.Pamphlet.Where(x => x.Deleted != true && x.Fild != null);
            var list2 = db.Pamphlet.Where(x => x.Deleted != true && x.Fild == null);
            foreach (var item in txt)
            {
                list1 = list1.Where(x => x.Code.Contains(item) || x.Course.ToLower().Contains(item.ToLower()) || x.Professor.Contains(item) || x.University.Name.Contains(item) || x.Fild.Contains(item));
                list2 = list2.Where(x => x.Code.Contains(item) || x.Course.ToLower().Contains(item.ToLower()) || x.Professor.Contains(item) || x.University.Name.Contains(item));
            }

            var result = list1.ToList();
            result.AddRange(list2.ToList());
            return result.OrderByDescending(x => x.DateCrt).OrderByDescending(x => x.View).Take(40).ToList();
        }
        public static IEnumerable<User> userSearch(string Text)
        {
            DBEntities db = new DBEntities();
            Text = ChangeText.changeNumber(Text);
           var result=db.User.ToList();

            var txt = Text.ToLower().Split(' ', '-', '/', '.', ',', '،');
            foreach (var item in txt)
            {
             result=db.User.Where(x => x.Name.Contains(item) || x.Mobile.Contains(Text)).ToList();
            }
            return result;
        }
    }
}