﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;

namespace University.Classes
{
    public class ValidateCaptcha
    {
        public static bool ValidateMyCaptcha(string response)
        {
            //secret that was generated in key value pair 
            string secret = ConfigurationManager.AppSettings["secretKey"];

            var client = new WebClient();
            var reply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);

            return Convert.ToBoolean(captchaResponse.Success);

        }
    }
}