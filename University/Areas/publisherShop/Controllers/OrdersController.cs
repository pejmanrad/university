﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using University.Models;

namespace University.Areas.publisherShop.Controllers
{
    [Authorize(Roles = "Admin,Publisher")]

    public class ordersController : Controller
    {
        // GET: publisherShop/orders
        DBEntities db = new DBEntities();
        public ActionResult Index()
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var publisher = user.PublisherShop.FirstOrDefault();
            //سفارشات
            var result = db.Order.Where(x => x.Pamphlet.PublisherShopID == publisher.ID && x.Step != 3 && x.Step != 0);
            result.ToList().ForEach(x => { x.Step = 2; x.Status = "در حال چاپ"; });//در دست بررسی
            db.SaveChanges();
            return View(result.OrderBy(x => x.DateCrt));
        }
        public string showdetails(Guid id)
        {
            var find = db.Order.FirstOrDefault(x => x.ID == id);
            if (find != null)
            {
                string result = "";
                result += $"<p class='modal-text-order'>کد درس: <span>{find.Pamphlet.Code}</span></p>";
                result += $"<p class='modal-text-order'>نام درس: <span>{find.Pamphlet.Course}</span></p>";
                result += $"<p class='modal-text-order'>رشته: <span>{find.Pamphlet.Fild}</span></p>";
                result += $"<p class='modal-text-order'>استاد: <span>{find.Pamphlet.Professor}</span></p>";
                result += $"<p class='modal-text-order'>زمان سفارش: <span>{find.DateCrt.ToString(" hh:mm yyyy/MM/dd ")}</span></p>";
                result += $"<p class='modal-text-order'>سفارش دهنده: <span>{find.User.Name}</span></p>";
                result += $"<p class='modal-text-order'>شماره: <span>{find.User.Mobile}</span></p>";
                return result;
            }
            return null;
        }
        public ActionResult LastOrder()
        {

            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var publisher = user.PublisherShop.FirstOrDefault();
            //سفارشات
            var result = db.Order.Where(x => x.Pamphlet.PublisherShopID == publisher.ID && x.Step == 3);
            return View(result.OrderByDescending(x => x.DateCrt).Take(50));
        }
        public int done(Guid id)
        {
            var order = db.Order.Find(id);
            if (order != null)
            {
                var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
                var publisher = user.PublisherShop.FirstOrDefault();
                if (order.Pamphlet.PublisherShopID == publisher.ID)
                {
                    order.Step = 3;
                    string smsUser = ConfigurationManager.AppSettings["SmsUser"];
                    string smsPass = ConfigurationManager.AppSettings["SmsPass"];
                    string smsPanelNo = ConfigurationManager.AppSettings["SmsPanel"];
                    string smsText = $@"سفارش شما با کد: {order.Pamphlet.Code} در سایت دانشگاه من آماده تحویل است.
از اینکه ما را انتخاب کردید مچکریم.";
                    SMS.ServiceSoapClient sendsms = new SMS.ServiceSoapClient();
          
                    string responsecode = sendsms.SendSMS(smsUser, smsPass, smsText, order.User.Mobile, smsPanelNo);
                    order.Status = "آماده تحویل!";
                    db.SaveChanges();
                    return 1;
                }
                return 0;
            }
            else
            {
                return 0;
            }

        }
    }
}