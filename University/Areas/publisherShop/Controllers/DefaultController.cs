﻿using InsertShowImage;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using University.Classes;
using University.Models;

namespace University.Areas.publisherShop.Controllers
{
    [Authorize(Roles = "Admin,Publisher")]

    public class DefaultController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: publisherShop/Default
        public ActionResult Index()
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var publisher = user.PublisherShop.FirstOrDefault();
            ViewBag.Name = publisher.Name;
            return View();
        }

        public ActionResult jozve(string Course = "", string Teacher = "", string Fild = "")
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var publisher = user.PublisherShop.FirstOrDefault();

            if (Course != "" || Teacher != "" || Fild != "")
            {
                List<Pamphlet> result;
                result = db.Pamphlet.Where(x => x.Course.Contains(Course) && x.Professor.Contains(Teacher) && x.Deleted != true).OrderByDescending(x => x.DateCrt).OrderByDescending(x => x.View).ToList();
                if (Fild != "")
                {
                    result = result.Where(x => x.Fild != null && x.Fild.Contains(Fild)).ToList();
                }
                return PartialView(result.Take(40));

            }
            var res = db.Pamphlet.Where(x => x.Deleted != true).OrderByDescending(x => x.DateCrt).ToList().Take(40);
           
            return PartialView(publisher.Pamphlet.Where(x => x.Deleted != true).OrderByDescending(x => x.DateCrt).ToList().Take(40));
        }

        public ActionResult advsearch(string Text)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                TempData["count"] = true;
                return PartialView("jozve", Search.advSearch(Text));
            }
            return null;
        }
        public ActionResult showdetails(Guid id)
        {
            var find = db.Pamphlet.FirstOrDefault(x => x.ID == id);
            if (find != null)
            {
                return PartialView(find);
            }
            return null;
        }

        public int orderCount()
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var publisher = user.PublisherShop.FirstOrDefault();
            //سفارشات
            var result = db.Order.Count(x => x.Pamphlet.PublisherShopID == publisher.ID && x.Step != 3 && x.Step != 0);

            return result;
        }
        public ActionResult create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult create(Pamphlet p,HttpPostedFileBase Image, HttpPostedFileBase file)
        {

            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var publisher = user.PublisherShop.FirstOrDefault();
            if (file != null)
            {
                if (checkPDF.isPdf(file))//چک کردن و ذخیره فایل
                {
                    string filename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                   
                    file.SaveAs(Server.MapPath("/pdf/" + filename));
                    p.Pdf = filename;
                }
                else
                {
                    ModelState.AddModelError("file", errorMessage: "فایل میباست به صورت pdf باشد ");
                    ViewBag.ErrorFile = true;
                    return View(p);
                }
            }
            if (Image!=null)
            {

                if (CheckContentImage.IsImage(Image))
                {
                    string ImageName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(Image.FileName);
                    Image.SaveAs(Server.MapPath("/Images/Preview/" + ImageName));
                    ImageResizer img = new ImageResizer();
                    img.Resize(Server.MapPath("/Images/Preview/" + ImageName), Server.MapPath("/Images/Preview/resize/" + ImageName));
                    System.IO.File.Delete(Server.MapPath("/Images/Preview/" + ImageName));
                    p.Image = ImageName;
                }
                else
                {
                    ModelState.AddModelError("Image", errorMessage: "فرمت تصویر تعریف نشده است ");
                    ViewBag.ErrorFile = true;
                    return View(p);
                }
            }
          
            Pamphlet pamphlet = new Pamphlet();
            pamphlet.ID = Guid.NewGuid();
            pamphlet.Code = ChangeText.changeNumber(p.Code); ;
            pamphlet.Course = ChangeText.changeNumber(p.Course).ToLower();
            pamphlet.Fild = p.Fild;
            pamphlet.DateCrt = DateTime.Now;
            pamphlet.DownloadCount = 0;
            pamphlet.DownloadPrice = ((p.DownloadPrice == null) ? 0 : p.DownloadPrice);
            pamphlet.Pdf = p.Pdf;
            pamphlet.Professor = p.Professor;
            pamphlet.PublisherShopID = publisher.ID;
            pamphlet.UniID = publisher.University.ID;
            pamphlet.Image = p.Image;
            updateSiteMap updateSiteMap = new updateSiteMap();
            updateSiteMap.UpdateSiteMap(ConfigurationManager.AppSettings["myDomain"] + "/order/print/" + pamphlet.ID, "add");
            db.Pamphlet.Add(pamphlet);
            db.SaveChanges();
            TempData["added"] = true;
            return RedirectToAction("create");
        }
        public ActionResult edit(Guid id)
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var publisher = user.PublisherShop.FirstOrDefault();
            var pamphlet = publisher.Pamphlet.ToList().Find(x => x.ID == id);
            if (pamphlet != null)
            {
                return View(pamphlet);
            }
            else
            {
                TempData["notfoundPamphlet"] = true;
                return RedirectToAction("index");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult edit(Pamphlet p, HttpPostedFileBase file, HttpPostedFileBase Images)
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var publisher = user.PublisherShop.FirstOrDefault();
            var pamphlet = publisher.Pamphlet.ToList().Find(x => x.ID == p.ID);
            if (pamphlet != null)
            {

                if (file != null)
                {
                    if (checkPDF.isPdf(file))//چک کردن و ذخیره فایل
                    {
                        if (System.IO.File.Exists(Server.MapPath("/pdf/" + pamphlet.Pdf)))
                            System.IO.File.Delete(Server.MapPath("/pdf/" + pamphlet.Pdf));
                        string filename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                        file.SaveAs(Server.MapPath("/pdf/" + filename));
                        p.Pdf = filename;
                    }
                    else 
                    {
                        ViewBag.ErrorFile = true;
                        return View(p);
                    }
                }
                if (Images != null)
                {

                    if (CheckContentImage.IsImage(Images))
                    {
                        if (System.IO.File.Exists(Server.MapPath("/Images/Preview/resize/" + pamphlet.Image)))
                            System.IO.File.Delete(Server.MapPath("/Images/Preview/resize/" + pamphlet.Image));
                        string ImageName = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(Images.FileName);
                        Images.SaveAs(Server.MapPath("/Images/Preview/" + ImageName));
                        ImageResizer img = new ImageResizer();
                        img.Resize(Server.MapPath("/Images/Preview/" + ImageName), Server.MapPath("/Images/Preview/resize/" + ImageName));
                        System.IO.File.Delete(Server.MapPath("/Images/Preview/" + ImageName));
                        p.Image = ImageName;
                    }
                    else
                    {
                        ModelState.AddModelError("Images", errorMessage: "فرمت تصویر تعریف نشده است ");
                        ViewBag.ErrorFile = true;
                        return View(p);
                    }
                }

                pamphlet.Pdf = p.Pdf;
                pamphlet.Image = p.Image;
                pamphlet.Code = ChangeText.changeNumber(p.Code);

                pamphlet.Fild = p.Fild;

                pamphlet.Course =ChangeText.changeNumber(p.Course).ToLower();

                pamphlet.Professor = p.Professor;
                db.SaveChanges();

                return RedirectToAction("index");
            }
            else
            {
                TempData["notfoundPamphlet"] = true;
                return RedirectToAction("index");
            }
        }
        public ActionResult Delete(Guid id)
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var publisher = user.PublisherShop.FirstOrDefault();
            var pamphlet = publisher.Pamphlet.ToList().Find(x => x.ID == id);
            if (pamphlet != null)
            {
                return PartialView(pamphlet);
            }
            else
            {
                return null;
            }
        }

        [HttpPost]

        public int deleteConfirm(Guid ID)
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var publisher = user.PublisherShop.FirstOrDefault();
            var pamphlet = publisher.Pamphlet.ToList().Find(x => x.ID == ID);
            if (pamphlet != null)
            {
                pamphlet.Deleted = true;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}