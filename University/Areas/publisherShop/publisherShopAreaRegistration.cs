﻿using System.Web.Mvc;

namespace University.Areas.publisherShop
{
    public class publisherShopAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "publisherShop";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "publisherShop_default",
                "publisherShop/{controller}/{action}/{id}",
                new { controller="default", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}