﻿using InsertShowImage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using University.Classes;
using University.Models;
using University.Models.ViewModel;

namespace University.Areas.User.Controllers
{
    [Authorize]

    public class DefaultController : Controller
    {
        DBEntities db = new DBEntities();
        public ActionResult Index()
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);

            var result = db.Order.Where(x => x.UserID == user.ID);
            return View(result.OrderByDescending(x => x.DateCrt));
        }
       
        public ActionResult UserProfile()
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            if (user.Credit != null && user.Credit > 10) 
            {
                ViewBag.credit = user.Credit;
            }
            return PartialView(user);
        }
      
        public ActionResult UserProfileChange()
        {
            UserProfileViewModel us = new UserProfileViewModel();
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            us.Name = user.Name;
            us.Email = user.Email;
            us.city = user.City;
            us.University = user.UniName;
            us.Fild = user.Fild;
          
            return PartialView(us);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserProfileChange(UserProfileViewModel us)
        {
            if (!ModelState.IsValid)
                return RedirectToAction("Index");

            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            user.Name = us.Name;
            user.Email = us.Email;
            user.City = us.city;
            user.UniName= us.University;
            user.Fild = us.Fild;
            db.SaveChanges();
            TempData["success"] = true;
            return RedirectToAction("Index");


        }
        public ActionResult changePassword()
        {
            return View();
        }
        [HttpPost]
        public int changePassword(ChangePassword change)
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name && x.Password == change.OldPass);
            if (user != null)
            {
                user.Password = change.RePass;
                db.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
        public ActionResult orders()
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var result = db.Order.Where(x => x.UserID == user.ID);
            return View(result.OrderByDescending(x => x.DateCrt));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult changepic(HttpPostedFileBase file)
        {
            if (CheckContentImage.IsImage(file))
            {
                var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
                if (System.IO.File.Exists(Server.MapPath("/Images/userImage/" + user.Picture)))
                    System.IO.File.Delete(Server.MapPath("/Images/userImage/" + user.Picture));
                string filename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                file.SaveAs(Server.MapPath("/Images/userImage/" + filename));

                ImageResizer img = new ImageResizer();
                img.Resize(Server.MapPath("/Images/userImage/" + filename), Server.MapPath("/Images/userImage/small/" + filename));
                System.IO.File.Delete(Server.MapPath("/Images/userImage/" + filename));

                user.Picture = filename;
                db.SaveChanges();
            }
            return RedirectToAction("index");


        }
     
    }
}