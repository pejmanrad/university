﻿using InsertShowImage;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using University.Classes;
using University.Models;

namespace University.Areas.User.Controllers
{
    [Authorize]
    public class NewssController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: User/News
        public ActionResult Index()
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);

            return View(user.News.Where(x=>x.hide!=true).ToList());
        }
        public ActionResult Add()
        {
            ViewBag.UniversityID = new SelectList(db.University, "ID", "Name");

            return View();
        }
        [HttpPost]
        public ActionResult Add(News news, bool showName, HttpPostedFileBase Image)
        {
            ViewBag.UniversityID = new SelectList(db.University, "ID", "Name");
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            if (Image != null)
            {
                if (CheckContentImage.IsImage(Image))//چک کردن و ذخیره فایل
                {
                    string filename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(Image.FileName);
                    Image.SaveAs(Server.MapPath("/Images/news/org/" + filename));
                    news.Image = filename;
                    ImageResizer img = new ImageResizer();
                    img.Resize(Server.MapPath("/Images/news/org/" + filename), Server.MapPath("/Images/news/" + filename));
                    System.IO.File.Delete(Server.MapPath("/Images/news/org/" + filename));
                }
                else
                {
                    ModelState.AddModelError("Image", errorMessage: "خطا در ارسال تصویر ");
                    ViewBag.ErrorFile = true;
                    ViewBag.UniversityID = new SelectList(db.University, "ID", "Name", news.UniversityID);

                    return View(news);
                }
            }
            else
            {
                ModelState.AddModelError("Image", errorMessage: "تصویری یافت نشد ");
                ViewBag.ErrorFile = true;
                ViewBag.UniversityID = new SelectList(db.University, "ID", "Name", news.UniversityID);

                return View(news);
            }
            news.ID = Guid.NewGuid();
            news.Like = 0;
            news.UserID = user.ID;
            news.ShowUser = showName;
            news.comment = true;
            news.Approve = false;
            news.DateCrt = DateTime.Now;
              updateSiteMap updateSiteMap = new updateSiteMap();
            updateSiteMap.UpdateSiteMap(ConfigurationManager.AppSettings["myDomain"] + "/n/" + news.ID + "/" + news.Title, "add");
            db.News.Add(news);
            db.SaveChanges();
            return RedirectToAction("index");
        }
        public ActionResult Edit(Guid id)
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var news = user.News.FirstOrDefault(x => x.ID == id);
            if (news != null)
            {
                ViewBag.UniversityID = new SelectList(db.University, "ID", "Name", news.UniversityID);
                return View(news);

            }
            else
            {
                return RedirectToAction("index");
            }
        }
        [HttpPost]
        public ActionResult Edit(News n,bool showName)
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var news = user.News.FirstOrDefault(x => x.ID == n.ID);
            if (news != null)
            {
                news.Title = n.Title;
                news.ShortDescription = n.ShortDescription;
                news.Description = n.Description;
                news.Tags = n.Tags;
                news.ShowUser = showName;
                news.UniversityID = n.UniversityID;
                news.Approve = n.Approve;

                db.SaveChanges();
                return RedirectToAction("index");
            }
            else
            {
                return RedirectToAction("index");
            }
        }
        public ActionResult delete(Guid id)
        {

            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var news = user.News.FirstOrDefault(x => x.ID == id);
            if (news != null)
            {
                return View(news);

            }
            else
            {
                return RedirectToAction("index");
            }
        }
        [HttpPost]
        [ActionName("delete")]
        public ActionResult deleteConfirm(Guid ID)
        {

            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var news = user.News.FirstOrDefault(x => x.ID == ID);
            if (news != null)
            {
                news.hide = true;
                db.SaveChanges();
                return RedirectToAction("index");


            }
            else
            {
                return RedirectToAction("index");
            }
        }
    }
}