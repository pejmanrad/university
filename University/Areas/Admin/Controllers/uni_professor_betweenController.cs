﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using University.Models;

namespace University.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class uni_professor_betweenController : Controller
    {
        private DBEntities db = new DBEntities();

        // GET: Admin/uni_professor_between
        public ActionResult Index()
        {
            var uni_professor_between = db.uni_professor_between.Include(u => u.Professor).Include(u => u.University);
            return View(uni_professor_between.ToList());
        }

        // GET: Admin/uni_professor_between/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            uni_professor_between uni_professor_between = db.uni_professor_between.Find(id);
            if (uni_professor_between == null)
            {
                return HttpNotFound();
            }
            return View(uni_professor_between);
        }

        // GET: Admin/uni_professor_between/Create
        public ActionResult Create()
        {
            ViewBag.ProfessorID = new SelectList(db.Professor, "ID", "Name");
            ViewBag.UniID = new SelectList(db.University, "ID", "Name");
            return View();
        }

        // POST: Admin/uni_professor_between/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,UniID,ProfessorID")] uni_professor_between uni_professor_between)
        {
            if (ModelState.IsValid)
            {
                db.uni_professor_between.Add(uni_professor_between);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProfessorID = new SelectList(db.Professor, "ID", "Name", uni_professor_between.ProfessorID);
            ViewBag.UniID = new SelectList(db.University, "ID", "Name", uni_professor_between.UniID);
            return View(uni_professor_between);
        }

        // GET: Admin/uni_professor_between/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            uni_professor_between uni_professor_between = db.uni_professor_between.Find(id);
            if (uni_professor_between == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProfessorID = new SelectList(db.Professor, "ID", "Name", uni_professor_between.ProfessorID);
            ViewBag.UniID = new SelectList(db.University, "ID", "Name", uni_professor_between.UniID);
            return View(uni_professor_between);
        }

        // POST: Admin/uni_professor_between/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,UniID,ProfessorID")] uni_professor_between uni_professor_between)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uni_professor_between).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProfessorID = new SelectList(db.Professor, "ID", "Name", uni_professor_between.ProfessorID);
            ViewBag.UniID = new SelectList(db.University, "ID", "Name", uni_professor_between.UniID);
            return View(uni_professor_between);
        }

        // GET: Admin/uni_professor_between/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            uni_professor_between uni_professor_between = db.uni_professor_between.Find(id);
            if (uni_professor_between == null)
            {
                return HttpNotFound();
            }
            return View(uni_professor_between);
        }

        // POST: Admin/uni_professor_between/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            uni_professor_between uni_professor_between = db.uni_professor_between.Find(id);
            db.uni_professor_between.Remove(uni_professor_between);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
