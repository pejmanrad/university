﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using University.Models;

namespace University.Areas.Admin.Controllers
{
    public class ContactUSController : Controller
    {
        private DBEntities db = new DBEntities();

        // GET: Admin/ContactUS
        public ActionResult Index()
        {
            var contactUS = db.ContactUS.Include(c => c.User);
            return View(contactUS.ToList());
        }
        public ActionResult ContactUsList()
        {
            
            return PartialView(db.ContactUS.ToList());
        }

        // GET: Admin/ContactUS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactUS contactUS = db.ContactUS.Find(id);
            if (contactUS == null)
            {
                return HttpNotFound();
            }
            return View(contactUS);
        }

        // GET: Admin/ContactUS/Create
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.User, "ID", "Name");
            return View();
        }

        // POST: Admin/ContactUS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Email,Title,Description,DateCrt,Read,UserID")] ContactUS contactUS)
        {
            if (ModelState.IsValid)
            {
                db.ContactUS.Add(contactUS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.User, "ID", "Name", contactUS.UserID);
            return View(contactUS);
        }

        // GET: Admin/ContactUS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactUS contactUS = db.ContactUS.Find(id);
            if (contactUS == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.User, "ID", "Name", contactUS.UserID);
            return View(contactUS);
        }

        // POST: Admin/ContactUS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Email,Title,Description,DateCrt,Read,UserID")] ContactUS contactUS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contactUS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.User, "ID", "Name", contactUS.UserID);
            return View(contactUS);
        }

        // GET: Admin/ContactUS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContactUS contactUS = db.ContactUS.Find(id);
            if (contactUS == null)
            {
                return HttpNotFound();
            }
            return View(contactUS);
        }

        // POST: Admin/ContactUS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ContactUS contactUS = db.ContactUS.Find(id);
            db.ContactUS.Remove(contactUS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
