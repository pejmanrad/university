﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using University.Classes;
using University.Models;

namespace University.Areas.Admin.Controllers
{

    [Authorize(Roles = "Admin")]
    public class adminNewsController : Controller 
    {
        DBEntities db = new DBEntities();
        public ActionResult Index() 
        { 
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            var khabar = db.News.OrderByDescending(x=>x.DateCrt).ToList();
            return View(khabar);
        }
        public ActionResult Create()
        {
            ViewBag.UniversityID = new SelectList(db.University, "ID", "Name");
            return View();
        }

        // POST: News/News/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.News news, HttpPostedFileBase Image)
        {


            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            if (Image != null)
            {
                if (CheckContentImage.IsImage(Image))//چک کردن و ذخیره فایل
                {
                    string filename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(Image.FileName);
                    Image.SaveAs(Server.MapPath("/Images/news/" + filename));
                    news.Image = filename;
                }
                else
                {
                    ModelState.AddModelError("Image", errorMessage: "تصویری یافت نشد ");
                    ViewBag.ErrorFile = true;
                    ViewBag.UniversityID = new SelectList(db.University, "ID", "Name", news.UniversityID);

                    return View(news);
                }
            }
            Models.News n = new Models.News()
            {
                Title = news.Title,
                ShortDescription = news.ShortDescription,
                DateCrt = DateTime.Now,
                Description = news.Description,
                Tags = news.Tags,
                ID = Guid.NewGuid(),
                Image = news.Image,
                Like = 0,
                UniversityID = news.UniversityID,
                Approve=true
            };

            updateSiteMap updateSiteMap = new updateSiteMap();
            updateSiteMap.UpdateSiteMap(ConfigurationManager.AppSettings["myDomain"] + "/n/" + n.ID + "/" + n.Title, "add");
            db.News.Add(n);
            db.SaveChanges();
            return RedirectToAction("index");
        }
        public ActionResult edit(Guid id)
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);

            var khabar = db.News.ToList().Find(x => x.ID == id);
            if (khabar != null)
            {
                ViewBag.UniversityID = new SelectList(db.University, "ID", "Name", khabar.UniversityID);

                return View(khabar);
            }
            else
            {
                return RedirectToAction("index");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult edit(Models.News n, HttpPostedFileBase file)
        {
            var khabar = db.News.ToList().Find(x => x.ID == n.ID);

            if (khabar != null)
            {
                if (file != null)
                {
                    if (CheckContentImage.IsImage(file))//چک کردن و ذخیره فایل
                    {
                        if (System.IO.File.Exists(Server.MapPath("/Images/news/" + khabar.Image)))
                            System.IO.File.Delete(Server.MapPath("/Images/news/" + khabar.Image));
                        string filename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                        file.SaveAs(Server.MapPath("/Images/news/" + filename));
                        n.Image = filename;
                    }
                    else
                    {
                        ViewBag.ErrorFile = true;
                        ViewBag.UniversityID = new SelectList(db.University, "ID", "Name", khabar.UniversityID);

                        return View(n);
                    }
                }
                khabar.Title = n.Title;
                khabar.ShortDescription = n.ShortDescription;
                khabar.Description = n.Description;
                khabar.Image = n.Image;
                khabar.Tags = n.Tags;
                db.SaveChanges();
               // updateSiteMap updateSiteMap = new updateSiteMap();
               // updateSiteMap.UpdateSiteMap(ConfigurationManager.AppSettings["myDomain"] + "/n/" + n.ID + "/" + n.Title, "edit");
                return RedirectToAction("index");
            }
            else
            {
                TempData["notfoundPamphlet"] = true;
                return RedirectToAction("index");
            }
        }
        public ActionResult Delete(Guid id)
        {
            var khabar = db.News.ToList().Find(x => x.ID == id);
            if (khabar != null)
            {
                return View(khabar);
            }
            else
            {
                return RedirectToAction("index");
            }
        }

        [HttpPost]
        [ActionName("delete")]
        [ValidateAntiForgeryToken]
        public ActionResult deleteConfirm(Guid ID)
        {

            var khabar = db.News.ToList().Find(x => x.ID == ID);
            if (khabar != null)
            {
                foreach (var item in khabar.NewsRate.ToList())
                {
                    db.NewsRate.Remove(item);
                };
                db.News.Remove(khabar);
                db.SaveChanges();
                return RedirectToAction("index");

            }
            else
            {
                return RedirectToAction("index");

            }
        }
    }
}