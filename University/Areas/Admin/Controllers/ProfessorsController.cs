﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using University.Classes;
using University.Models;

namespace University.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class ProfessorsController : Controller
    {
        private DBEntities db = new DBEntities();

        // GET: University/Professors
        public ActionResult Index()
        {
            var professor = db.Professor.Include(p => p.User);
            return View(professor.ToList());
        }

        // GET: University/Professors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Professor professor = db.Professor.Find(id);
            if (professor == null)
            {
                return HttpNotFound();
            }
            return View(professor);
        }

        // GET: University/Professors/Create
        public ActionResult Create()
        {
            ViewBag.uni = db.University.ToList();
            return View();
        }

        // POST: University/Professors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,Description,Image")] Professor professor, HttpPostedFileBase Image, List<int> uni)
        {
            if (ModelState.IsValid)
            {
                if (Image != null)
                {
                    if (CheckContentImage.IsImage(Image))//چک کردن و ذخیره فایل
                    {
                        string filename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(Image.FileName);
                        Image.SaveAs(Server.MapPath("/Images/professor/" + filename));
                        professor.Image = filename;
                    }
                    else
                    {
                        ModelState.AddModelError("Image", errorMessage: "تصویری یافت نشد ");
                        ViewBag.ErrorFile = true;
                        ViewBag.uni = db.University.ToList();

                        return View(professor);
                    }
                }
                db.Professor.Add(professor);
                foreach (var item in uni)
                {
                    db.uni_professor_between.Add(new uni_professor_between()
                    {
                        ProfessorID = professor.ID,
                        UniID = item
                    });
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(professor);
        }

        // GET: University/Professors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Professor professor = db.Professor.Find(id);
            if (professor == null)
            {
                return HttpNotFound();
            }
            ViewBag.uni = db.University.ToList();
            return View(professor);
        }

        // POST: University/Professors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Description,Image")] Professor professor, HttpPostedFileBase file, List<int> uni)
        {
            if (ModelState.IsValid)
            {
                if (CheckContentImage.IsImage(file))//چک کردن و ذخیره فایل
                {
                    if (System.IO.File.Exists(Server.MapPath("/Images/professor/" + professor.Image)))
                        System.IO.File.Delete(Server.MapPath("/Images/professor/" + professor.Image));
                    string filename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                    file.SaveAs(Server.MapPath("/Images/professor/" + filename));
                    professor.Image = filename;
                }
                else
                {
                    ModelState.AddModelError("Image", errorMessage: "تصویر با فرمت درست آپلود  کنید");
                    ViewBag.uni = db.University.ToList();

                    return View(professor);
                }
                foreach (var item in professor.uni_professor_between)
                {
                    db.uni_professor_between.Remove(item);
                }
                foreach (var item in uni)
                {
                    db.uni_professor_between.Add(new uni_professor_between()
                    {
                        ProfessorID = professor.ID,
                        UniID = item
                    });
                }

                db.Entry(professor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.uni = db.University.ToList();

            return View(professor);
        }

        // GET: University/Professors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Professor professor = db.Professor.Find(id);
            if (professor == null)
            {
                return HttpNotFound();
            }
            return View(professor);
        }

        // POST: University/Professors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Professor professor = db.Professor.Find(id);
            foreach (var item in professor.uni_professor_between.ToList())
            {
                db.uni_professor_between.Remove(item);
            }
            db.Professor.Remove(professor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
