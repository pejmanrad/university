﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using University.Models;

namespace University.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class PublisherShopsController : Controller
    {
        private DBEntities db = new DBEntities();

        // GET: Admin/PublisherShops
        public ActionResult Index()
        {
            var publisherShop = db.PublisherShop.Include(p => p.University).Include(p => p.User);
            return View(publisherShop.ToList());
        }

        // GET: Admin/PublisherShops/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PublisherShop publisherShop = db.PublisherShop.Find(id);
            if (publisherShop == null)
            {
                return HttpNotFound();
            }
            return View(publisherShop);
        }

        // GET: Admin/PublisherShops/Create
        public ActionResult Create()
        {
            ViewBag.UniID = new SelectList(db.University, "ID", "Name");
            ViewBag.UserID = new SelectList(db.User, "ID", "Name");
            return View();
        }

        // POST: Admin/PublisherShops/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Owner,ShebaAccount,Address,UserID,UniID")] PublisherShop publisherShop)
        {
            if (ModelState.IsValid)
            {
                db.PublisherShop.Add(publisherShop);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UniID = new SelectList(db.University, "ID", "Name", publisherShop.UniID);
            ViewBag.UserID = new SelectList(db.User, "ID", "Name", publisherShop.UserID);
            return View(publisherShop);
        }

        // GET: Admin/PublisherShops/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PublisherShop publisherShop = db.PublisherShop.Find(id);
            if (publisherShop == null)
            {
                return HttpNotFound();
            }
            ViewBag.UniID = new SelectList(db.University, "ID", "Name", publisherShop.UniID);
            ViewBag.UserID = new SelectList(db.User, "ID", "Name", publisherShop.UserID);
            return View(publisherShop);
        }

        // POST: Admin/PublisherShops/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Owner,ShebaAccount,Address,UserID,UniID")] PublisherShop publisherShop)
        {
            if (ModelState.IsValid)
            {
                db.Entry(publisherShop).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UniID = new SelectList(db.University, "ID", "Name", publisherShop.UniID);
            ViewBag.UserID = new SelectList(db.User, "ID", "Name", publisherShop.UserID);
            return View(publisherShop);
        }

        // GET: Admin/PublisherShops/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PublisherShop publisherShop = db.PublisherShop.Find(id);
            if (publisherShop == null)
            {
                return HttpNotFound();
            }
            return View(publisherShop);
        }

        // POST: Admin/PublisherShops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PublisherShop publisherShop = db.PublisherShop.Find(id);
            db.PublisherShop.Remove(publisherShop);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
