﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using University.Models;

namespace University.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin")]
    public class PamphletsController : Controller
    {
        private DBEntities db = new DBEntities();

        // GET: Admin/Pamphlets
        public ActionResult Index()
        {
            var pamphlet = db.Pamphlet.OrderByDescending(x=>x.View).Take(40);
            return View(pamphlet.ToList());
        }

        // GET: Admin/Pamphlets/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pamphlet pamphlet = db.Pamphlet.Find(id);
            if (pamphlet == null)
            {
                return HttpNotFound();
            }
            return View(pamphlet);
        }

        // GET: Admin/Pamphlets/Create
        public ActionResult Create()
        {
            ViewBag.PublisherShopID = new SelectList(db.PublisherShop, "ID", "Name");
            ViewBag.UniID = new SelectList(db.University, "ID", "Name");
            return View();
        }

        // POST: Admin/Pamphlets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Code,Course,Fild,PrintPrice,DownloadPrice,DownloadCount,DateCrt,Status,Pdf,Professor,PublisherShopID,UniID,Deleted,Edited,Image,View")] Pamphlet pamphlet)
        {
            if (ModelState.IsValid)
            {
                pamphlet.ID = Guid.NewGuid();
                db.Pamphlet.Add(pamphlet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PublisherShopID = new SelectList(db.PublisherShop, "ID", "Name", pamphlet.PublisherShopID);
            ViewBag.UniID = new SelectList(db.University, "ID", "Name", pamphlet.UniID);
            return View(pamphlet);
        }

        // GET: Admin/Pamphlets/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pamphlet pamphlet = db.Pamphlet.Find(id);
            if (pamphlet == null)
            {
                return HttpNotFound();
            }
            ViewBag.PublisherShopID = new SelectList(db.PublisherShop, "ID", "Name", pamphlet.PublisherShopID);
            ViewBag.UniID = new SelectList(db.University, "ID", "Name", pamphlet.UniID);
            return View(pamphlet);
        }

        // POST: Admin/Pamphlets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Code,Course,Fild,PrintPrice,DownloadPrice,DownloadCount,DateCrt,Status,Pdf,Professor,PublisherShopID,UniID,Deleted,Edited,Image,View")] Pamphlet pamphlet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pamphlet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PublisherShopID = new SelectList(db.PublisherShop, "ID", "Name", pamphlet.PublisherShopID);
            ViewBag.UniID = new SelectList(db.University, "ID", "Name", pamphlet.UniID);
            return View(pamphlet);
        }

        // GET: Admin/Pamphlets/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pamphlet pamphlet = db.Pamphlet.Find(id);
            if (pamphlet == null)
            {
                return HttpNotFound();
            }
            return View(pamphlet);
        }

        // POST: Admin/Pamphlets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Pamphlet pamphlet = db.Pamphlet.Find(id);
            db.Pamphlet.Remove(pamphlet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
