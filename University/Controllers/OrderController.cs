﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using University.Models;
using University.Models.ViewModel;

namespace University.Controllers
{
    public class OrderController : Controller
    {
        public string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }
        DBEntities db = new DBEntities();

        // GET: Order
        //public ActionResult download(Guid id)
        //{
        //    var us = User.Identity;
        //    if (us.IsAuthenticated)
        //    {
        //        var user = db.User.FirstOrDefault(x => x.Cookie == us.Name);
        //        if (user != null)
        //        {

        //            if (user.RoleID == 2) TempData["publisher"] = true;
        //        }
        //        else
        //        {
        //            return Redirect("/account/login/");
        //        }
        //    }
        //    var find = db.Pamphlet.FirstOrDefault(x => x.ID == id);
        //    if (find == null)
        //    {
        //        ViewBag.notfound = true;
        //    }
        //    return View(find);
        //}
       
        public ActionResult print(Guid id)
        {
            var find = db.Pamphlet.FirstOrDefault(x => x.ID == id);
            if (find != null)
            {
                db.PamphletView.Add(new PamphletView()
                {
                    ID = Guid.NewGuid(),
                    DateViw = DateTime.Now,
                    IPAddress = GetIp(),
                    PamphletID = find.ID,
                    Show=true

                });
                db.SaveChanges();
                return View(find);
            }
                ViewBag.notfound = true;
            return View();
        }
#region comment
        public ActionResult createComment(Guid id, Guid? ParentID)
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
                return PartialView(new pamphlet_CommentViewModel()
                {
                    PamphletID = id,
                    ParentID = ParentID,
                    user=user
                });
            }
            return null;

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public string createComment(pamphlet_CommentViewModel comment)
        {

            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);

            PamphletComment co = new PamphletComment()
            {
                ParentID = comment.ParentID,
                Date = DateTime.Now,
                Text = comment.Text,
                PamphletID = comment.PamphletID,
                UserID = user.ID,
                ID = Guid.NewGuid(),

            };
            db.PamphletComment.Add(co);
            db.SaveChanges();
            string res = @"<div class='alert alert-success text-center'>دیدگاه شما با موفقیت ثبت شد</div>";
            return res;
        }
        public ActionResult showComments(Guid id)
        {
            return PartialView(db.PamphletComment.Where(x => x.PamphletID == id && x.ParentID == null).OrderByDescending(x=>x.Date));
        }
#endregion

        [Authorize]
        public ActionResult Payment(Guid id)
        {
            var find = db.Pamphlet.FirstOrDefault(x => x.ID == id);
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            if (find != null)
            {
                Order order = new Order()
                {
                    ID = Guid.NewGuid(),
                    DateCrt = DateTime.Now,
                    IsFinaly = false,
                    UserID = user.ID,
                    PamphletID = find.ID,
                    Step = 0,
                    Status = "در انتظار پرداخت"
                };
                db.Order.Add(order);
                db.SaveChanges();
                //Session["orderID"] = order.ID;

                var amount = int.Parse(ConfigurationManager.AppSettings["Amount"]);
                if (user.Credit>=amount)
                {
                    user.Credit -= amount;
                    order.IsFinaly = true;
                    order.Step = 1;
                    order.Status += $"=> پرداخت از اعتبار  {DateTime.Now}";
                    db.SaveChanges();
                    ViewBag.credit=true;
                    string smsUser = ConfigurationManager.AppSettings["SmsUser"];
                    string smsPass = ConfigurationManager.AppSettings["SmsPass"];
                    string smsPanelNo = ConfigurationManager.AppSettings["SmsPanel"];

                    string smsText = $@"{order.Pamphlet.Code}-{order.Pamphlet.Course}-{order.Pamphlet.Professor}
برای:{order.User.Name}
{order.User.Mobile}";
                    SMS.ServiceSoapClient sendsms = new SMS.ServiceSoapClient();
                    string responsecode = sendsms.SendSMS(smsUser, smsPass, smsText, "09195888177", smsPanelNo);
                    return View("callback");
                }
                Response.Cookies["OrderID"].Value = order.ID.ToString();
                return RedirectToAction("pardakht");
            }
                return View();
        }
        public ActionResult Pardakht() 
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public ActionResult sendPay()
        {
            var oId = Request.Cookies["OrderID"].Value.ToString();
            Guid orderid = Guid.Parse(oId);
            var order = db.Order.Find(orderid);
            if(order != null)
            {
                var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);

                string callback = ConfigurationManager.AppSettings["myDomain"] + "/order/callback/";
                var zarinCode = ConfigurationManager.AppSettings["ZarinCode"];
                string description = $"{order.Pamphlet.Code},{order.Pamphlet.Course}";
                System.Net.ServicePointManager.Expect100Continue = false;
                zarinpal.PaymentGatewayImplementationService zp = new zarinpal.PaymentGatewayImplementationService();
                string authority;
                var amount = int.Parse(ConfigurationManager.AppSettings["Amount"]);
                int status = zp.PaymentRequest(zarinCode, amount, description, user.Email, user.Mobile, callback, out authority);
                if (status == 100)
                {
                    Response.Redirect($"https://www.zarinpal.com/pg/StartPay/{authority}/Sep");
                }
                else
                {
                    ViewBag.error = "status code:" + status;
                }

            }
            else
            {
                ViewBag.error = "یافت نشد";

            }
            return View("pardakht");
        }
        public ActionResult callback()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Authority"]) && !string.IsNullOrEmpty(Request.QueryString["Status"]))
            {

                if (Request.QueryString["Status"].ToString() == "OK")
                {
                    try
                    {
                        System.Net.ServicePointManager.Expect100Continue = false;
                        zarinpal.PaymentGatewayImplementationService zp = new zarinpal.PaymentGatewayImplementationService();
                        long refID;
                        string Authority = Request.QueryString["Authority"];
                        string oID = Request.Cookies["OrderID"].Value.ToString();
                        Guid orderid = Guid.Parse(oID);

                        var order = db.Order.Find(orderid);
                        Response.Cookies["OrderID"].Expires = DateTime.Now.AddHours(-6);
                        var zarinCode = ConfigurationManager.AppSettings["ZarinCode"];
                        int Amount = int.Parse(ConfigurationManager.AppSettings["Amount"]);
                        int status = zp.PaymentVerification(zarinCode, Authority, Amount, out refID);
                        if (status == 100)
                        {
                            order.IsFinaly = true;
                            order.Step = 1;
                            order.Status += $"=> موفقیت  {DateTime.Now}";
                            db.SaveChanges();
                            ViewBag.Status = true;
                            string smsUser = ConfigurationManager.AppSettings["SmsUser"];
                            string smsPass = ConfigurationManager.AppSettings["SmsPass"];
                            string smsPanelNo = ConfigurationManager.AppSettings["SmsPanel"];
                            string smsText = $@"{order.Pamphlet.Code}-{order.Pamphlet.Course}
{order.User.Name}
{order.User.Mobile}";
                            SMS.ServiceSoapClient sendsms = new SMS.ServiceSoapClient();
                            string responsecode = sendsms.SendSMS(smsUser, smsPass, smsText, "09195888177", smsPanelNo);
                        }
                        else
                        {
                            order.Status += $"=> خطا  {DateTime.Now}";
                            db.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {

                        ViewBag.Error = ex.Message;
                    }
                }
            }
            return View();
        }
    }
}