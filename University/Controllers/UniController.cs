﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using University.Models;

namespace University.Controllers
{
    public class UniController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: Uni
        public ActionResult Index(string id)
        {
            var us = User.Identity;
            if (us.IsAuthenticated)
            {
                var user = db.User.FirstOrDefault(x => x.Cookie == us.Name);
                if (user.RoleID == 2) TempData["publisher"] = true;
            }
            List<Models.University> uni = new List<Models.University>();
            if (id != null)
            {
                var city = db.City.FirstOrDefault(x => x.Name == id);//جستجوی شهر
                if (city != null)
                {
                    uni = city.University.ToList();
                }
            }
            else
            {
                uni = db.University.ToList();
            }

            return View(uni);
        }

        [Route("f/{id}")]//مسیر ارسال اطلاعات
        public ActionResult Find(string id)//یافتن دانشگاه مورد نظر
        {
            int uniID = int.Parse(id.Split('-')[1]);
            var uni = db.University.FirstOrDefault(x => x.ID == uniID);//جستجوی دانشکاه بر اساس نام 
            if (uni != null)
            {
                return View(uni);
            }
            return RedirectToAction("Index");
        }
        public ActionResult Professor(string id)
        {
            return View();
        }
        [Authorize]
        public ActionResult follow(int id)
        {
            var uni = db.University.Find(id);
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            if(!db.Uni_Follower.Any(x=>x.UserID==user.ID && x.UniID == uni.ID))
            {
                db.Uni_Follower.Add(new Uni_Follower()
                {
                    UserID = user.ID,
                    UniID = uni.ID,
                    DateReg = DateTime.Now,
                    ID = Guid.NewGuid()
                });
                db.SaveChanges();
            }
         
            return Redirect($"/f/{uni.Name}-{uni.ID}");
        }
    }
}