﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using University.Classes;
using University.Models;
using University.Models.ViewModel;

namespace University.Controllers
{
    public class AccountController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: Account
        public string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }
        public ActionResult register()
        {
            return View();
        }
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel register)
        {
            if (!ModelState.IsValid)
                return View(register);
            register.Mobile = ChangeText.changeNumber(register.Mobile);
            bool isCapthcaValid;

            isCapthcaValid = ValidateCaptcha.ValidateMyCaptcha(Request["g-recaptcha-response"]);

            if (isCapthcaValid)
            {
                var userExist = db.User.FirstOrDefault(x => x.Mobile == register.Mobile);
                if (userExist == null)
                {
                    Random rnd = new Random();

                    Models.User user = new Models.User()
                    {
                        Name = register.Name,
                        Mobile = register.Mobile,
                        Password = register.Password,
                        DateReg = DateTime.Now,
                        RoleID = 3,
                        Cookie = Guid.NewGuid().ToString(),
                        IpAddress = GetIp(),
                        Sms = rnd.Next(10000, 100000).ToString(),
                        SmsDate = DateTime.Now,
                        Approved = false
                    };
                    SMS.ServiceSoapClient sendsms = new SMS.ServiceSoapClient();
                    string smsUser = ConfigurationManager.AppSettings["SmsUser"];
                    string smsPass = ConfigurationManager.AppSettings["SmsPass"];
                    string smsPanelNo = ConfigurationManager.AppSettings["SmsPanel"];
                    string smsText = $"کد فعالسازی شما در سایت دانشگاه من :{user.Sms}";

                    string responsecode = sendsms.SendSMS(smsUser, smsPass, smsText, user.Mobile, smsPanelNo);

                    db.User.Add(user);
                    if (responsecode == "0" || responsecode == "2")
                    {
                        db.SaveChanges();
                        TempData["UserID"] = user.Cookie;
                        return RedirectToAction("approve", "account");
                    }
                    else
                    {
                        ViewBag.smsError = responsecode;
                        return View(register);
                    }
                }
                else
                {
                    if (userExist.Approved != true)
                    {
                        string smsUser = ConfigurationManager.AppSettings["SmsUser"];
                        string smsPass = ConfigurationManager.AppSettings["SmsPass"];
                        string smsPanelNo = ConfigurationManager.AppSettings["SmsPanel"];
                        SMS.ServiceSoapClient sendsms = new SMS.ServiceSoapClient();
                        Random rnd = new Random();
                        userExist.Sms = rnd.Next(10000, 100000).ToString();
                        userExist.SmsDate = DateTime.Now;
                        string smsText = $"کد فعالسازی شما در سایت دانشگاه من :{userExist.Sms}";
                        string responsecode = sendsms.SendSMS(smsUser, smsPass, smsText, userExist.Mobile, smsPanelNo);
                        db.SaveChanges();
                        TempData["UserID"] = userExist.Cookie;
                        return RedirectToAction("approve", "account");
                    }
                    ModelState.AddModelError("Mobile", "شماره همراه وارد شده موجود است");
                    return View(register);
                }
            }
            else
            {

                ModelState.AddModelError("CaptchaInputText", "لطفا تاییدیه را تیک بزنید");
                return View(register);
            }

        }
        public ActionResult approve()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult approve(string UserID, string Code)
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == UserID);
            if (user != null)
            {
                if (user.SmsDate.Value.AddMinutes(2) > DateTime.Now)
                {
                    Code = ChangeText.changeNumber(Code);
                    if (user.Sms == Code)
                    {
                        string ReturnUrl = "/";
                        user.Approved = true;
                        user.loginCount = 0;
                        user.Credit = 1000;
                        db.SaveChanges();
                        FormsAuthentication.SetAuthCookie(user.Cookie, false);
                        HttpCookie orderUrl = Request.Cookies["OrderUrl"];

                        if (orderUrl.Value != null)
                        {
                            ReturnUrl = orderUrl.Value;
                            var c = new HttpCookie("OrderUrl");
                            c.Expires = DateTime.Now.AddDays(-1);
                            Response.Cookies.Add(c);
                        }
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        System.Threading.Thread.Sleep(3000);
                        ViewBag.notfound = true;
                    }

                }
                else
                {
                    string smsUser = ConfigurationManager.AppSettings["SmsUser"];
                    string smsPass = ConfigurationManager.AppSettings["SmsPass"];
                    string smsPanelNo = ConfigurationManager.AppSettings["SmsPanel"];
                    SMS.ServiceSoapClient sendsms = new SMS.ServiceSoapClient();
                    Random rnd = new Random();
                    user.Sms = rnd.Next(10000, 100000).ToString();
                    user.SmsDate = DateTime.Now;
                    string smsText = $"کد فعالسازی شما در سایت دانشگاه من : {user.Sms}";
                    string responsecode = sendsms.SendSMS(smsUser, smsPass, smsText, user.Mobile, smsPanelNo);
                    db.SaveChanges();
                    ViewBag.EndTime = true;
                    System.Threading.Thread.Sleep(2000);
                    TempData["UserID"] = user.Cookie;
                    return View();
                }
            }
            else
            {
                System.Threading.Thread.Sleep(3000);
                ViewBag.notfound = true;
            }
            return View();
        }

        public ActionResult Login(string ReturnUrl = "/")
        {
            HttpCookie cookie = Request.Cookies["mycookie"];
            if (ReturnUrl != "/")
            {
                HttpCookie rtnUrl = new HttpCookie("OrderUrl");
                rtnUrl.Value = ReturnUrl.ToString();//your problem hear.
                rtnUrl.Expires = DateTime.Now.AddMinutes(20);
                rtnUrl.HttpOnly = true;
                Response.Cookies.Add(rtnUrl);
            }
            if (!User.Identity.IsAuthenticated)
                if (cookie != null)
                {
                    var user = db.User.SingleOrDefault(x => x.Cookie == cookie.Value.ToString());
                    if (user != null)
                    {

                        FormsAuthentication.SetAuthCookie(user.Cookie, true);
                        return Redirect(ReturnUrl);
                    }
                }
            return View();



        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel login, string ReturnUrl = "/")
        {
            if (!ModelState.IsValid)
                return View();
            bool isCapthcaValid;
            if (Session["captcha"] != null)
            {
                isCapthcaValid = ValidateCaptcha.ValidateMyCaptcha(Request["g-recaptcha-response"]);
            }
            else
            {
                isCapthcaValid = true;
            }
            if (isCapthcaValid)
            {
                login.Mobile = ChangeText.changeNumber(login.Mobile);
                var user = db.User.FirstOrDefault(x => x.Mobile == login.Mobile.Trim().ToLower() && x.Password == login.Password);
                if (user != null)
                {
                    if (user.Approved != true)
                    {

                        string smsUser = ConfigurationManager.AppSettings["SmsUser"];
                        string smsPass = ConfigurationManager.AppSettings["SmsPass"];
                        string smsPanelNo = ConfigurationManager.AppSettings["SmsPanel"];
                        SMS.ServiceSoapClient sendsms = new SMS.ServiceSoapClient();
                        Random rnd = new Random();
                        user.Sms = rnd.Next(10000, 100000).ToString();
                        user.SmsDate = DateTime.Now;
                        string smsText = $"کد فعالسازی شما در سایت دانشگاه من :{user.Sms}";
                        string responsecode = sendsms.SendSMS(smsUser, smsPass, smsText, user.Mobile, smsPanelNo);
                        db.SaveChanges();
                        TempData["UserID"] = user.Cookie;
                        return RedirectToAction("approve", "account");
                    }
                    user.Cookie = Guid.NewGuid().ToString();
                    user.Status = "اخرین ورود : " + DateTime.Now.ToString();
                    if (user.loginCount == 1 || user.loginCount == null)
                    {
                        user.loginCount = 1;
                    }
                    else
                    {
                       user.loginCount+=1;
                    }
                    db.SaveChanges();

                    FormsAuthentication.SetAuthCookie(user.Cookie, login.Remember);

                    if (login.Remember)
                    {

                        HttpCookie cookie = new HttpCookie("mycookie");
                        cookie.Value = user.Cookie;//your problem hear.
                        cookie.Expires = DateTime.Now.AddDays(90);
                        cookie.HttpOnly = true;
                        Response.Cookies.Add(cookie);
                        //Response.Cookies["login"].Value = user.Cookie.ToString();

                    }

                    if (user.RoleID == 2)
                    {
                        if (ReturnUrl == "/")
                            ReturnUrl = "/publishershop/";

                    }
                    HttpCookie orderUrl = Request.Cookies["OrderUrl"];

                    if (orderUrl != null)
                    {
                        ReturnUrl = orderUrl.Value;
                        var c = new HttpCookie("OrderUrl");
                        c.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(c);
                    }
                    return Redirect(ReturnUrl);

                }
                else
                {
                    Session["captcha"] = true;
                    ModelState.AddModelError("Mobile", "کاربری با این مشخصات یافت نشد");
                    return View(login);
                }
            }
            else
            {
                ModelState.AddModelError("Captcha", "لطفا تیک را بزنید");
                return View(login);
            }

        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            if (Request.Cookies["mycookie"] != null)
            {
                var c = new HttpCookie("mycookie");
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }
            return Redirect("/");
        }
        public ActionResult RecoveryPass()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RecoveryPass(RecoveryPassViewModel recovery)
        {
            bool isCapthcaValid = ValidateCaptcha.ValidateMyCaptcha(Request["g-recaptcha-response"]);
            if (isCapthcaValid)
            {

                var user = db.User.FirstOrDefault(x => x.Mobile == recovery.Mobile);
                if (user != null)
                {
                    string smsUser = ConfigurationManager.AppSettings["SmsUser"];
                    string smsPass = ConfigurationManager.AppSettings["SmsPass"];
                    string smsPanelNo = ConfigurationManager.AppSettings["SmsPanel"];
                    SMS.ServiceSoapClient sendsms = new SMS.ServiceSoapClient();
                    Random rnd = new Random();
                    user.Sms = rnd.Next(10000, 100000).ToString();
                    user.SmsDate = DateTime.Now;
                    string smsText = $"کد فراموشی کلمه عبور در سایت دانشگاه من : {user.Sms}";
                    string responsecode = sendsms.SendSMS(smsUser, smsPass, smsText, user.Mobile, smsPanelNo);
                    db.SaveChanges();

                    var sms = new sendsmsViewModel() { Mobile = user.Mobile };
                    return View("~/Views/Account/sendSms.cshtml", sms);

                }
                else
                {
                    ModelState.AddModelError("Mobile", " یافت نشد");
                    return View(recovery);
                }
            }
            else
            {
                ModelState.AddModelError("Captcha", " تیک تاییدیه را بزنید");
                return View(recovery);
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult changepass(sendsmsViewModel code)
        {
            var user = db.User.FirstOrDefault(x => x.Mobile == code.Mobile);
            if (user != null)
            {
                if (user.SmsDate.Value.AddMinutes(2) > DateTime.Now)
                {
                    code.sms = ChangeText.changeNumber(code.sms);
                    if (user.Sms == code.sms)
                    {
                        return View(new changepassViewModel() { ActiveCode = user.Cookie });
                    }
                    else
                    {
                        ViewBag.sms = true;
                    }

                }
                else
                {
                    ViewBag.expire = true;
                    return View();

                }
            }
            else
            {
                ViewBag.notfound = true;
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult changepassconfirm(changepassViewModel model)
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == model.ActiveCode);
            if (user != null)
            {
                user.Password = model.Pass;
                user.Cookie = Guid.NewGuid().ToString();
                db.SaveChanges();
                FormsAuthentication.SetAuthCookie(user.Cookie, false);
                return Redirect("/");
            }
            else
            {
                ViewBag.error = true;
                return View();
            }
        }

    }
}