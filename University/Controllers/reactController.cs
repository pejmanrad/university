﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using University.Models;

namespace University.Controllers
{
    public class reactController : Controller
    {
        // GET: react
        DBEntities db = new DBEntities();
        class jsonres
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string Image { get; set; }
        }
        class login
        {
            public Boolean status{ get; set; }
            public string ID { get; set; }
        }
        public JsonResult Index()
        {
        
            List<jsonres> j = new List<jsonres>();
            foreach (var item in db.News.ToList())
            {
                j.Add(new jsonres()
                {
                    Id = item.ID,
                    Name = item.Title,
                    Image=item.Image

                });
            }
            return Json(j,JsonRequestBehavior.AllowGet);
        }
        public JsonResult Login(string phone,string pass)
        {
            var user = db.User.FirstOrDefault(x => x.Mobile == phone.Trim() && x.Password == pass);
            login log = new login();
            if (user != null)
            {
                log.ID="true";
            }
            else
            {
                log.ID = "false";
            }
            return Json(log,JsonRequestBehavior.AllowGet);

        }
    }
}