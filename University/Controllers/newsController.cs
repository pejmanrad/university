﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using University.Models;
using University.Models.ViewModel;

namespace University.Controllers
{
    public class newsController : Controller
    {
        DBEntities db = new DBEntities();
        public string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }
        [Route("khabar")]
        public ActionResult Index()
        {            
            return View(db.News.Where(x=>x.hide!=true&&x.Approve!=false).OrderByDescending(x => x.DateCrt).Take(20));
        }


        [Route("n/{id}/{string}")]
        public ActionResult news(Guid id)
        {
            var news = db.News.Where(x => x.hide != true && x.Approve != false).OrderByDescending(x => x.DateCrt).FirstOrDefault(x => x.ID == id);
            if (news != null)
            {
                return View(news);
            }
            ViewBag.notfound = true;
            return View();
        }
        public int Like(Guid id)
        {
            var news = db.News.OrderByDescending(x => x.DateCrt).FirstOrDefault(x => x.ID == id);
            if (news.Like!=null)
            {
                news.Like++;
            }
            else
            {
                news.Like = 1;
            }
            db.SaveChanges();
            return 1;
        }
        public  int Rate(Guid id, string r)
        {
            var news = db.News.OrderByDescending(x => x.DateCrt).FirstOrDefault(x => x.ID == id);
            if (news.NewsRate.Any(x => x.IpAdrs == GetIp()))
            { 
                return 0;
            }
            else { 
            NewsRate nr = new NewsRate()
            {
                ID = Guid.NewGuid(),
                newsID = news.ID,
                rate = short.Parse(r),
                IpAdrs=GetIp()
            };
            db.NewsRate.Add(nr);
            db.SaveChanges();
            return 1;
            }

        }
        public ActionResult createComment(Guid id)
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
                return PartialView(new NewsCommentViewModel()
                {
                    ID = Guid.NewGuid(),
                    NewsID=id,
                    user=user
                    
                });
            }
            return null;
        }
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public string createComment(NewsCommentViewModel comment)
        {
            var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
            NewsComment co = new NewsComment()
            {
                NewsID=comment.NewsID,
                Date = DateTime.Now,
                Text = comment.Text,
                UserID = user.ID,
                ID = Guid.NewGuid(),
            };
            db.NewsComment.Add(co);
            db.SaveChanges();
            string res = @"<div class='alert alert-success text-center'>دیدگاه شما با موفقیت ثبت شد</div>";
            return res;
        }
    
        public ActionResult showComments(Guid id)
        {
            return PartialView(db.NewsComment.Where(x => x.NewsID == id ).OrderByDescending(x=>x.Date));
        }

    }
}