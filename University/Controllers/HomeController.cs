﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using University.Classes;
using University.Models;

namespace University.Controllers
{
    public class HomeController : Controller
    {
        public string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }
        DBEntities db = new DBEntities();
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult jozve(string Course = "", string Teacher = "", string Fild = "", string Uni = "")
        {


            if (Uni != "" && Uni != "انتخاب دانشگاه")
            {
                var university = db.University.FirstOrDefault(x => x.Name.Trim() == Uni.Trim());
                if (Course != "" || Teacher != "" || Fild != "")
                {
                    List<Pamphlet> result;
                    result = db.Pamphlet.Where(x => x.Course.Contains(Course) && x.Professor.Contains(Teacher) && x.Deleted != true).OrderByDescending(x => x.DateCrt).OrderByDescending(x => x.View).ToList();
                    if (Fild != "")
                    {
                        result = result.Where(x => x.Fild != null && x.Fild.Contains(Fild)).ToList();
                    }
                    return PartialView(result.OrderByDescending(x => x.DateCrt).OrderByDescending(x => x.View).ToList().Take(40));

                }
                var res = university.Pamphlet.Where(x => x.Deleted != true).OrderByDescending(x => x.DateCrt).OrderByDescending(x => x.View).ToList().Take(40);
                return PartialView(res);

            }
            else
            {
                if (Course != "" || Teacher != "" || Fild != "")
                {
                    List<Pamphlet> result;
                    result = db.Pamphlet.Where(x => x.Course.Contains(Course) && x.Professor.Contains(Teacher) && x.Deleted != true).OrderByDescending(x => x.DateCrt).OrderByDescending(x => x.View).ToList();
                    if (Fild != "")
                    {
                        result = result.Where(x => x.Fild != null && x.Fild.Contains(Fild)).ToList();
                    }
                    return PartialView(result.OrderByDescending(x => x.DateCrt).OrderByDescending(x => x.View).ToList().Take(40));

                }
                var res = db.Pamphlet.Where(x => x.Deleted != true).OrderByDescending(x => x.DateCrt).OrderByDescending(x => x.View).ToList().Take(40);
                return PartialView(res);

            }

            var jozve = db.Pamphlet.Where(x => x.Deleted != true).OrderByDescending(x => x.DateCrt).OrderByDescending(x => x.View).ToList().Take(40);
            return PartialView(jozve);
        }
        public string jozveCount(string Uni = "", string Course = "", string Teacher = "", string Fild = "")
        {
            string count = "";
            if (Uni != "" && Uni != "انتخاب دانشگاه")
            {
                var university = db.University.FirstOrDefault(x => x.Name.Trim() == Uni.Trim());
                if (Course != "" || Teacher != "" || Fild != "")
                {
                    List<Pamphlet> result;
                    result = db.Pamphlet.Where(x => x.Course.Contains(Course) && x.Professor.Contains(Teacher) && x.Deleted != true).OrderByDescending(x => x.DateCrt).OrderByDescending(x => x.View).ToList();
                    if (Fild != "")
                    {
                        result = result.Where(x => x.Fild != null && x.Fild.Contains(Fild)).ToList();
                    }
                    count = result.Count().ToString();

                }
            }
            else
            {
                if (Course != "" || Teacher != "" || Fild != "")
                {
                    List<Pamphlet> result;
                    result = db.Pamphlet.Where(x => x.Course.Contains(Course) && x.Professor.Contains(Teacher) && x.Deleted != true).OrderByDescending(x => x.DateCrt).OrderByDescending(x => x.View).ToList();
                    if (Fild != "")
                    {
                        result = result.Where(x => x.Fild != null && x.Fild.Contains(Fild)).ToList();
                    }
                    count = result.Count().ToString();


                }


            }
            if (count != "")
                return count + " مورد یافت شد.";
            else return null;
        }

        public ActionResult advsearch(string Text)
        {
            if (!String.IsNullOrEmpty(Text))
            {
                TempData["count"] = true;
                return PartialView("jozve", Search.advSearch(Text));
            }
            return null;
        }
        public ActionResult Pamphlet(Guid id)
        {
            var find = db.Pamphlet.FirstOrDefault(x => x.ID == id);
            if (find != null)
            {
                if (find.View == null)
                    find.View = 1;
                else
                    find.View++;

                db.PamphletView.Add(new PamphletView()
                {
                    ID = Guid.NewGuid(),
                    DateViw = DateTime.Now,
                    IPAddress = GetIp(),
                    PamphletID = find.ID

                });
                db.SaveChanges();



                return PartialView(find);
            }

            return null;
        }
        public ActionResult slider()
        {
            return PartialView();
        }

        //public string province()
        //{
        //    var result = @"<div class='row'>";
        //    foreach (var item in db.Province.ToList())
        //    {
        //        result += $@" <div class='col-sm-4'>
        //                            <div class='modal-btn' onclick='selectProvince(this)'>{item.Name}</div>
        //                    </div>";
        //    }
        //    result += "</div>";
        //    return result;
        //}
        //public string city(string id)
        //{
        //    string result = "<div class='row'>";
        //    var province = db.Province.FirstOrDefault(x => x.Name == id);
        //    if (province != null)
        //    {
        //        if (province.City.Any())
        //        {
        //            foreach (var item in province.City.ToList())
        //            {
        //                result += $@"<div class='col-sm-6'>
        //                            <div class='modal-btn' onclick='selectCity(this)'>{item.Name}</div>
        //                    </div>";
        //            }
        //            result += "</div>";

        //            return result;
        //        }
        //        else
        //        {
        //            return "undefined";
        //        }
        //    }
        //    return null;
        //}

        public ActionResult university()
        {
            var university = db.University.ToList();
            return PartialView(university);

        }

        [Route("aboutus")]
        public ActionResult About()
        {
            return View();
        }
        [Authorize]
        [Route("contact")]
        public ActionResult Contact()
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        [Route("contact")]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactUS cu)
        {
            bool isCapthcaValid = ValidateCaptcha.ValidateMyCaptcha(Request["g-recaptcha-response"]);
            if (isCapthcaValid)
            {

                var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
                ContactUS contact = new ContactUS()
                {
                    Name = cu.Name,
                    DateCrt = DateTime.Now,
                    Description = cu.Description,
                    Email = cu.Email,
                    Read = false,
                    Title = cu.Title,
                    UserID = user.ID
                };
                db.ContactUS.Add(contact);
                db.SaveChanges();
                ViewBag.success = true;
            }
            else
            {
                ModelState.AddModelError("Captcha", "لطفا تیک را بزنید");
                return View(cu);
            }
            return View();

        }
        [Route("zalipour")]
        public ActionResult project()
        {
            return View();
        }
        public ActionResult Menu()
        {
            var us = User.Identity;
            if (us.IsAuthenticated)
            {
                var user = db.User.FirstOrDefault(x => x.Cookie == User.Identity.Name);
                if (user != null)
                {

                    return PartialView(user);
                }
                FormsAuthentication.SignOut();
                Response.Cookies["login"].Expires = DateTime.Now.AddDays(-1);

                ViewBag.login = true;
            }
            return PartialView();

        }

    }
}
