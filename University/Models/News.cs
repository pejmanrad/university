//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace University.Models
{
    using System.ComponentModel.DataAnnotations;
    using System;
    using System.Collections.Generic;
    
    [MetadataType(typeof(NewsMetadata))]
    public partial class News
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public News()
        {
            this.NewsComment = new HashSet<NewsComment>();
            this.newsIpAddress = new HashSet<newsIpAddress>();
            this.NewsRate = new HashSet<NewsRate>();
        }
    
        public System.Guid ID { get; set; }
        public string Title { get; set; }
        public System.DateTime DateCrt { get; set; }
        public int Like { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Tags { get; set; }
        public int UniversityID { get; set; }
        public Nullable<bool> hide { get; set; }
        public Nullable<bool> comment { get; set; }
        public Nullable<bool> Approve { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<bool> ShowUser { get; set; }
    
        public virtual University University { get; set; }
        public virtual User User { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NewsComment> NewsComment { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<newsIpAddress> newsIpAddress { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NewsRate> NewsRate { get; set; }
    }
}
