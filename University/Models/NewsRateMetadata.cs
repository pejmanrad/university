﻿namespace University.Models
{
    internal class NewsRateMetadata
    {
        public System.Guid ID { get; set; }
        public System.Guid newsID { get; set; }
        public short rate { get; set; }
    }
}