﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace University.Models.ViewModel
{
    public class NewsCommentViewModel
    {
        public System.Guid ID { get; set; }
     
        public System.Guid NewsID { get; set; }
        public string Text { get; set; }

        public User user { get; set; }
    }
}