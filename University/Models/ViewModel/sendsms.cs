﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace University.Models.ViewModel
{
    public class sendsmsViewModel
    {
         public string Mobile { get; set; }
        [Display(Name = "کد دریافتی")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        [RegularExpression("(^[(0-9|۰-۹)]{5}$)", ErrorMessage = "لطفا کد دریافتی را به درستی وارد کنید")]

        public string sms { get; set; }
    }
}