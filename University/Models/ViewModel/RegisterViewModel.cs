﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace University.Models.ViewModel
{
    public class RegisterViewModel
    {

        [Display(Name = "نام کامل")]
        [Required(ErrorMessage = "لطفا {0} راوارد کنید")]
        public string Name { get; set; }

        [Display(Name = "شماره همراه")]
        [Required(ErrorMessage = "لطفا {0} راوارد کنید")]
        [RegularExpression("(^(09|۰۹)[(0-9|۰-۹)]{9}$)", ErrorMessage = "لطفا شماره همراه را به درستی وارد کنید")]
        public string Mobile { get; set; }
        [Display(Name = "کلمه عبور")]
        [Required(ErrorMessage = "لطفا {0} راوارد کنید")]
        [DataType(DataType.Password)]
        [MinLength(4,ErrorMessage ="برای امنیت بیشتر حداقل 4 کاراکتر لازم است")]
        public string Password { get; set; }
  
    }
}