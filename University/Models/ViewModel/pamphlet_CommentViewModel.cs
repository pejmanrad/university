﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace University.Models.ViewModel
{
    public class pamphlet_CommentViewModel
    {
        [Display(Name = "جزوه")]

        [Required(ErrorMessage = "لطفا {0} راوارد کنید")]

        public System.Guid PamphletID { get; set; }

        [Display(Name = "متن")]
        [Required(ErrorMessage = "لطفا {0} راوارد کنید")]
        public string Text { get; set; }
        public Nullable<System.Guid> ParentID { get; set; }
        public User user { get; set; }
    }
}