﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace University.Models.ViewModel
{
    public class RecoveryPassViewModel
    {
        [Display(Name = "شماره همراه")]
        [Required(ErrorMessage = "لطفا {0} را وارد کنید")]
        [RegularExpression("(^(09|۰۹)[(0-9|۰-۹)]{9}$)", ErrorMessage = "لطفا شماره همراه را به درستی وارد کنید")]

        public string Mobile { get; set; }
    }
}