﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace University.Models.ViewModel
{
    public class UserProfileViewModel
    {
        [Display(Name = "نام کامل")]
        [Required(ErrorMessage = "لطفا {0} راوارد کنید")]
        public string Name { get; set; }

        [Display(Name = "شهر")]
        public string city { get; set; }


        [Display(Name = "رشته ")]
        public string Fild { get; set; }

        [Display(Name = "نام دانشگاه")]
        public string University { get; set; }

        [Display(Name = "ایمیل")]
        public string Email { get; set; }
 
    }
}