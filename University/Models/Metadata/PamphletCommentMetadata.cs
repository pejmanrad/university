﻿using System.ComponentModel.DataAnnotations;

namespace University.Models
{
    internal class PamphletCommentMetadata
    {
        [Key]
        public System.Guid ID { get; set; }
        [Display(Name = "مشخصه کاربر")]
        public int UserID { get; set; }
        [Display(Name = "مشخصه جزوه")]
        public System.Guid PamphletID { get; set; }
        [Display(Name = "متن")]
        [MaxLength(128, ErrorMessage = "بیش از حد مجاز")]
        public string Text { get; set; }
        [Display(Name = "تاریخ")]
        public System.DateTime Date { get; set; }
        [Display(Name = "والد")]
        public System.Guid? ParentID { get; set; }


    }
}